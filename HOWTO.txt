The following repositories were copied and modified for our convenience: 

segway_rmp: https://github.com/segwayrmp/segway_rmp.git
serial: https://github.com/wjwwood/serial.git
libsegwayrmp: https://github.com/segwayrmp/libsegwayrmp.git

Check updates on them in case of problems on our current maintained version.

*** How to compile and run segway stuff ***

* Compiling *

1. Install libserial-dev:
sudo apt-get install libserial-dev

2. add packages contained in this folder in your catkin workspace/src

3. compile from the catkin workspace root
   $ catkin_make


* Setup *

1. copy the files in udev folder into '/etc/udev/rules.d'
   $ sudo cp udev/*.rules /etc/udev/rules.d

2. copy the ros_run.sh script into /etc/ros
   $ sudo cp udev/ros_run.sh /etc/ros

3. connect the robot to the laptop and turn it on

4. check with 'dmesg' that it is linked to /dev/ttyUSB0


* ADDITIONAL INSTRUCTIONS (may be needed for USB3 ports) *
- if you don't have a udev rule, just write on your terminal:
   (1) sudo chmod a+w /dev/ttyUSBX
   (2) sudo chown YOUR_USERNAME /dev/ttyUSBX
   (3) sudo chgrp YOUR_USERNAME /dev/ttyUSBX
- if you are using usb3 you might still have troubles:
   * run lsusb on the terminal
   * identify the bus and device number corresponding to the robot
   * execute (1), (2), (3) substituting /dev/ttyUSBX with /dev/bus/usb/BUSNUMBER/DEVICENUMBER

   
   
* Test with segwayrmp_gui *

1. connect the robot to the laptop and turn it on (green button to turn it on and yellow button to enable the motors)

2. run segwayrmp_gui

   $ roscd libsegwayrmp
   $ cd bin
   $ ./segwayrmp_gui

3. select "serial", RMP100 and the port /devv/ttyUSB?? to match your configuration

4. connect a joystick and figure out which axis are which

5. click on "connect" and ignore the first checksum error you get

6. steer the robot (remember to check the "send to robot button")/

  *NEVER* modify the autobalance settings. This will destroy the robot.
  
  *** OPTIMAL CONFIGURATION VALUES FOR DIAGO ***
  
  Maximum velocity = 0.8
  Maximum turn rate = 0.8
  
  Send these values with "Send Configuration" button. 
  Note that the values are stored in the segway board,
  but they are not read again when you run segwayrmp_gui next time.
  
  For setting acceleration limits, use the parameters in the launch file segway_rmp.launch
  
  
  
* Run the ros node *

1. connect the robot and turn it on

2. if port is not /dev/ttyUSB0 edit launch file segway_rmpX/launch/segway_rmp.launch
   to set the serial_port parameter.

3. launch the node

   $ roslaunch segway_rmpX segway_rmp.launch

   The robot should come up as a traditional erratic.



*** OPTIMAL CONFIGURATION VALUES FOR DIAGO ***

  <param name="linear_forward_vel_limit" value="1.0" />
  <param name="linear_backward_vel_limit" value="0.5" />
  <param name="angular_vel_limit" value="90.0" />

  <param name="linear_pos_accel_limit" value="0.5" />
  <param name="linear_neg_accel_limit" value="0.5" />
  <param name="angular_pos_accel_limit" value="30.0" />
  <param name="angular_neg_accel_limit" value="70.0" />
  

To drive the robot with joystick and gradient_based_navigation,
see instructions in the gradient_based_navigation package
(https://github.com/Imperoli/gradient_based_navigation).

        
        
*** IF THE ROBOT DOES NOT MOVE ***

1. Check with segwayrmp_gui in libsegwayrmp

  libsegwayrmp/bin$ ./segwayrmp_gui 

2. Select Serial  RMP100  /dev/ttyUSB0  and then 'Connect'
You will see values of parameters changing quickly (e.g., 10 Hz)

3. If you have a joystick, set the axis, otherwise go ahead.

4. Check 'Send to segway' and you should see the numbers chaning quickly as before.
If not, the connection is wrong (try to restart the robot)

5. If you have a joystick try to move the robot.

6. Uncheck 'Send to segway', Disconnect and quit the program.
If the program does not quit, the connection is wrong (try to restart the robot)

7. If you close the program correctly after disconnecting successfully, it should work.
"Error: Checksum mismatch..." can happen do not worry too much about it.




